module "swarm" {
  source          = "github.com/adarlan/terraform-swarm-aws"
  base_name       = "vegas"
  is_production   = false
  instance_type   = "t2.micro"
  base_domain     = local.base_domain
  subdomain_label = "vegas"
  deployment_config = {
    git_platform = "github"
    git_url      = "https://github.com/adarlan/traefik-swarm-deployment.git"
    git_ref      = "main"
  }
  tags = {
    Environment = "test"
  }
}
